
classdef myopticalFlow

   
    properties (SetAccess='private', GetAccess='public', Dependent = true)
        Vx; 
        Vy;
        Magnitude;
    end
    
    properties (Access='private')
        pVx;
        pVy;
    end
    
    methods %Getters
        function out = get.Vx(this)
            out = this.pVx;
        end        
        %--------------------------
        function out = get.Vy(this)
            out = this.pVy;
        end     
        %--------------------------
        function out = get.Magnitude(this)
            out = computeMagnitude(this.pVx, this.pVy);
        end     
    end 
    
    %-----------------------------------------------------------------------
    methods (Access='public')
        function this = myopticalFlow(varargin)
            %Constructor
            if (nargin ~= 0) && (nargin ~= 2)
            error('numArgInvalid');
            elseif (nargin == 0)
                this.pVx = zeros(0,1);
                this.pVy = zeros(0,1);
            elseif nargin == 2
                checkVelocityComponent(varargin{1}, 'Vx', 1);
                checkVelocityComponent(varargin{2}, 'Vy', 2);
                crossCheckSizes(varargin{1}, varargin{2});
                crossCheckDataTypes(varargin{1}, varargin{2});
                this.pVx = varargin{1};
                this.pVy = varargin{2};
            end
        end
        
        %------------------------------------------------------------------
        function varargout = plot(this, varargin)
           
            if (~isSimMode())
                error('plotNotSupported');
            end
            
            nargoutchk(0,1);
            [h, inputs] = parsePlotInputs(varargin{:});
            XDecimationFactor = inputs.DecimationFactor(1);
            YDecimationFactor = inputs.DecimationFactor(2);

            borderOffset = 1;

            [R, C] = size(this.Vx);
            RV = borderOffset:YDecimationFactor:(R-borderOffset+1);   
            CV = borderOffset:XDecimationFactor:(C-borderOffset+1);   
            [X, Y] = meshgrid(CV,RV);

            velocityX = this.Vx;
            velocityY = this.Vy;
            
            tmpVx = velocityX(RV,CV);
            tmpVy = velocityY(RV,CV);
            tmpVx = tmpVx.*inputs.ScaleFactor;
            tmpVy = tmpVy.*inputs.ScaleFactor;
 
            quiver(h, X(:), Y(:), tmpVx(:), tmpVy(:), 0); 
            
            if nargout == 1
                varargout{1} = h;
            end            
        end        
    end
end    


function [h, inputs] = parsePlotInputs(varargin)

% Parse the PV pairs
parser = inputParser;

parser.addParameter('Parent', [], ...
    @validateAxesHandle)

parser.addParameter('DecimationFactor', [1 1], @checkDecimationFactor);
parser.addParameter('ScaleFactor', 1, @checkScaleFactor);

% Parse input
parser.parse(varargin{:});

% Assign returns
h = parser.Results.Parent;

if isempty(h)
    h = gca;
end

inputs.DecimationFactor = parser.Results.DecimationFactor;
inputs.ScaleFactor  = parser.Results.ScaleFactor;

end


function  crossCheckSizes(Vx, Vy)
    if ~isequal(size(Vx), size(Vy))
        error('VxVySizeMismatch');
    end
end

function  checkVelocityComponent(V, name, id)
   validateattributes(V,{'double','single'}, ...
    {'real','nonsparse','2d'}, mfilename, name, id);
end

function  crossCheckDataTypes(Vx, Vy)
    if ~isequal(class(Vx), class(Vy))
        error('VxVyClassMismatch');
    end
end

function  checkDecimationFactor(decimFactor)
    validateattributes(decimFactor, {'numeric'}, ...
    {'nonempty', 'integer', 'nonsparse', 'vector', 'numel', 2, '>', 0}, ...
    mfilename, 'DecimationFactor');
end

function  checkScaleFactor(scaleFactor)
    validateattributes(scaleFactor, {'numeric'}, ...
    {'nonempty', 'integer', 'nonsparse', 'scalar', '>=', 1}, ...
    mfilename, 'ScaleFactor');
end

function issim = isSimMode()
    issim = isempty(coder.target);
end

function val = validateAxesHandle(ax)
val = true;

if ~(isscalar(ax) && ishghandle(ax,'axes'))
    error('invalidAxesHandle');
end
end

function mag = computeMagnitude(Vx, Vy)
    mag = sqrt(Vx.*Vx + Vy.*Vy);
end